/*
Entry point to redux store
A store holds the whole state tree of your application.
The only way to change the state inside it is to dispatch an action to it
user -> action -> reducer -> store 
*/

import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk'
import rootReducer from './reducers'

const initialState = {}

const middleware = [thunk]

const composeEnhancers = window.REDUX_DEVTOOLS_EXTENSION_COMPOSE || compose;
const enhancer = composeEnhancers(applyMiddleware(...middleware));

const store = createStore(
  rootReducer,
  initialState,
  enhancer
);

export default store