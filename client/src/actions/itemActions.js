import axios from 'axios'
import { GET_ITEMS, ADD_ITEM, DELETE_ITEM, ITEMS_LOADING } from './types'


export const getItems = () => dispatch  => {
  // this return checks the action.type in the reducer itemReducer
  dispatch(setItemsLoading())
  axios
    .get('/api/itemsRoute')
    .then(res =>
       dispatch({
         type: GET_ITEMS, payload: res.data
        })
    )
}

export const deleteItem = (id) => dispatch => {
  // this return checks the action.type in the reducer itemReducer
  axios
    .delete(`/api/itemsRoute/${id}`)
    .then(res => 
      dispatch({
        type: DELETE_ITEM,
        payload: id
      })
    )
}

export const addItem = (item) => dispatch => {
  // this return checks the action.type in the reducer itemReducer
  axios
  .post('/api/itemsRoute', item)
  .then(res =>
     dispatch({
       type: ADD_ITEM,
       payload: res.data
     })
  )
}

export const setItemsLoading = () => {
  return {
    type: ITEMS_LOADING
  }
}