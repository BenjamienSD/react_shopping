const express = require('express');
const router = express.Router();

// Item Model
const Item = require('../../models/ItemModel')

// @route   GET api/itemsRoute
// @desc    GET all items
// @access  Public

router.get('/', (req, res) => {
    ItemModel.find()
        .sort({ date: -1 })
        .then(items => res.json(items))
})

// @route   POST api/itemsRoute
// @desc    Create a item
// @access  Public

router.post('/', (req, res) => {
  const newItem = new Item({
    name: req.body.name
  })
  newItem
    .save()
    .then(item => res.json(item))
})

// @route   DELETE api/itemsRoute/:id
// @desc    Delete a item
// @access  Public

router.delete('/:id', (req, res) => {
  Item.findById(req.params.id)
    .then(item => item.remove()
    .then(() => res.json({succes: true})))
    .catch(err => res.status(404).json({succes: false}))
})


module.exports = router